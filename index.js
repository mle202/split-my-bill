const button = document.querySelector("#btn");
button.addEventListener("click",calculateAmount);

const buttonTip = document.querySelector("#AddTip");
buttonTip.addEventListener("click",showTip);

function showTip (e){
    e.preventDefault();
    tip.style.display = "block";
}

function calculateAmount(e) {
    e.preventDefault();
    const bill = document.querySelector("#bill").value;
    const people = document.querySelector("#people").value;
    const tip = document.querySelector("#tip").value;

    if (bill === "" || people === "" || people ==="") {
        Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Please enter your information!',
          });
    }
//1- how much will pay one person
    let amountPerPerson = bill/people;
    console.log(amountPerPerson);

    //2- how much tips will pay one person

    let tipPerPerson = (bill* tip)/people;
    console.log(tipPerPerson);

    // 3 how much total per person

    let totalSum = amountPerPerson + tipPerPerson;
    console.log (totalSum);

    //toFix
    amountPerPerson = amountPerPerson.toFixed(2);

    //  show in the app

    document.querySelector("#dividedBill").textContent = amountPerPerson;
    document.querySelector("#divdedTip").textContent = tipPerPerson;
    document.querySelector("#BillAndTip").textContent = totalSum;
}